<?php

/*
	-----------------------------------------------------------
	FILE NAME: Division.class.php

	Copyright (c) 2016 Miami University, All Rights Reserved.

	Miami University grants you ("Licensee") a non-exclusive, royalty free,
	license to use, modify and redistribute this software in source and
	binary code form, provided that i) this copyright notice and license
	appear on all copies of the software; and ii) Licensee does not utilize
	the software in a manner which is disparaging to Miami University.

	This software is provided "AS IS" and any express or implied warranties,
	including, but not limited to, the implied warranties of merchantability
	and fitness for a particular purpose are disclaimed. It has been tested
	and is believed to work as intended within Miami University's
	environment. Miami University does not warrant this software to work as
	designed in any other environment.

	AUTHOR: Erin Mills

	DESCRIPTION:

	INPUT:
	PARAMETERS:

	ENVIRONMENT DEPENDENCIES: RESTNG FRAMEWORK

	TABLE USAGE:
	

	AUDIT TRAIL:

	DATE    PRJ-TSK          UniqueID
	Description:

	08/16/2016		millse
	Description:	Initial Program
	
 */

namespace MiamiOH\RestngIaDivision\Services;

class Division extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $database = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_GEN_IAPROD'; // secure datasource


    /************************************************/
    /**********Setter Dependency Injection***********/
    /***********************************************/

    // Inject the datasource object provided by the framework
    public function setDataSource($datasource)
    {
        $this->dataSource = $datasource;
    }

    // Inject the database object provided by the framework
    public function setDatabase($database)
    {
        $this->database = $database;
    }

    // Inject the configuration object provided by the framework
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    // GET(read/view) the Ballot Information
    public function getDivisions()
    {

        //log
        $this->log->debug('Division service was called.');

        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $divisionCode = null;
        $payload = array();

        //get a database handle for the IA database
        $dbh = $this->database->getHandle($this->datasource_name);

        if (isset($options['divisionCode'])) {

            //if the divisionCode is empty, throw an error
            if ($options['divisionCode'] == '') {
                throw new \Exception('Error: divisionCode cannot be empty');
            }

            //if the divisionCode is malicious/invalid, throw an error
            if (preg_match('/[^A-Za-z0-9() :\-_"\']/', $options['divisionCode'])) {
                throw new \Exception('Error: divisionCode is invalid');
            }

            $divisionCode = $options['divisionCode'];
        }


        $queryString = '
          select 
            distinct STANDARDIZED_DIVISION_NM, 
            standardized_division_cd 
          from Mudwmgr.Dim_Std_Division_Department
          where standardized_division_cd is not null';

        if ($divisionCode) {//if there is a divisionCode filter to apply, add it.
            $queryString = $queryString . ' and standardized_division_cd = ?';
        }

        $queryString = $queryString . ' order by Standardized_Division_Nm';

        if ($divisionCode) {
            $results = $dbh->queryall_array($queryString, $divisionCode);
        } else {
            $results = $dbh->queryall_array($queryString);
        }


        //put all the results into the payload
        $payload = $this->divisionMakeover($results);

        // Response was successful and Return information
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);
        return $response;

    }

    private function divisionMakeover($divisions)
    {
        $prettyDivisions = array();

        foreach ($divisions as $d) {
            $prettyD = array();
            $prettyD['divisionCode'] = $d['standardized_division_cd'];
            $prettyD['divisionDescription'] = $d['standardized_division_nm'];
            $prettyDivisions[] = $prettyD;
        }

        return $prettyDivisions;
    }

}
