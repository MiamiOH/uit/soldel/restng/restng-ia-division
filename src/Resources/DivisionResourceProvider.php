<?php

namespace MiamiOH\RestngIaDivision\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class DivisionResourceProvider extends ResourceProvider
{

    private $tag = "IA";
    private $dot_path = "IA.Division";
    private $s_path = "/ia/division/v1";
    private $bs_path = '\Division';

    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.Division',
            'type' => 'object',
            'properties' => array(
                'divisionCode' => array('type' => 'string', 'description' => 'Code of the division'),
                'divisionDescription' => array('type' => 'string', 'description' => 'Description of the division'),
                //'departments' => array('type' => 'string', 'description' => 'Departments that belong to the division. Maybe an array of injected departments?'),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.Division.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Division',
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Division',
            'class' => 'MiamiOH\RestngIaDivision\Services' . $this->bs_path,
            'description' => 'This service provides resources about Divisions from IA.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
                'dataSource' => array('type' => 'service', 'name' => 'APIDataSourceFactory'),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
                'action' => 'read', //GET
                'name' => $this->dot_path . '.get',
                'description' => 'Return divisions',
                'pattern' => $this->s_path,
                'service' => 'Division',
                'method' => 'getDivisions',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'options' => array(
                    'divisionCode' => array('type' => 'single', 'description' => 'Division code to filter by'),
                ),
//        'middleware' => array(
//            'authenticate' => array('type' => 'token'),
//            'authorize' => array(
//                array('type' => 'authMan',
//                    'application' => 'WebServices',
//                    'module' => 'FacultyElection',
//                    'key' => array('view')
//                )
//            )
//        ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Collection of divisions',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Division.Collection',
                        )
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}