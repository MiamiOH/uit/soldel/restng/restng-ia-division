<?php

/*
-----------------------------------------------------------
FILE NAME: getDivisionTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Erin Mills

DESCRIPTION: 


ENVIRONMENT DEPENDENCIES: 
RESTng Framework
PHPUnit

TABLE USAGE:

Web Service Usage:

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

05/31/2016              millse
Description:            Initial Draft
			 
-----------------------------------------------------------
*/

namespace MiamiOH\RestngIaDivision\Tests\Unit;


class GetDivisionTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $api, $request, $dbh, $division;


    // set up method automatically called by PHPUnit before every test method:
    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'getResourceParam'))
            ->getMock();

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->division = new \MiamiOH\RestngIaDivision\Services\Division();
        $this->division->setDatabase($db);
        $this->division->setLogger();
        $this->division->setDatasource($ds);
        $this->division->setRequest($this->request);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    /*
      *   Tests Case where no Filter is Given
      *   Actual: No Election ID option specified.
      *	  Expected Return: At least one election ID must be specified.
      */
    public function testNoFilter()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryAllDivisions')));

        $response = $this->division->getDivisions();
        $this->assertEquals($this->mockExpectedAllDivisionsResponse(), $response->getPayload());

    }

    public function mockExpectedAllDivisionsResponse()
    {
        $expectedReturn =
            array(
                array(
                    'divisionCode' => 'CAS',
                    'divisionDescription' => 'College of Arts & Science',
                ),
                array(
                    'divisionCode' => 'COE',
                    'divisionDescription' => 'College of Engineering',
                ),
                array(
                    'divisionCode' => 'CLS',
                    'divisionDescription' => 'College of Library Science',
                ),
            );

        return $expectedReturn;
    }

    public function mockQueryAllDivisions()
    {
        return array(
            array(
                'standardized_division_nm' => 'College of Arts & Science',
                'standardized_division_cd' => 'CAS',
            ),
            array(
                'standardized_division_nm' => 'College of Engineering',
                'standardized_division_cd' => 'COE',
            ),
            array(
                'standardized_division_nm' => 'College of Library Science',
                'standardized_division_cd' => 'CLS',
            ),
        );
    }

    public function mockNoParameters()
    {
        $optionsArray = array();
        return $optionsArray;
    }

    public function testDivisionCodeFilter()
    {
        $this->request->expects($this->once())->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsDivisionFilter')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryDivisionFilter')));

        $response = $this->division->getDivisions();
        $this->assertEquals($this->mockExpectedDivisionFilterResults(), $response->getPayload());
    }

    public function mockOptionsDivisionFilter()
    {
        return array(
            'divisionCode' => 'CAS'
        );
    }

    public function mockQueryDivisionFilter()
    {
        return array(
            array(
                'standardized_division_nm' => 'College of Arts & Science',
                'standardized_division_cd' => 'CAS',
            )
        );
    }

    public function mockExpectedDivisionFilterResults()
    {
        $expectedReturn =
            array(
                array(
                    'divisionCode' => 'CAS',
                    'divisionDescription' => 'College of Arts & Science',
                )
            );
        return $expectedReturn;
    }

    public function testDivisionCodeFilterEmpty()
    {
        $this->request->expects($this->once())->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsDivisionFilterEmpty')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        try {
            $response = $this->division->getDivisions();
            $this->fail();
        } catch (\Exception $e) {
            $this->assertEquals('Error: divisionCode cannot be empty', $e->getMessage());
        }
    }

    public function mockOptionsDivisionFilterEmpty()
    {
        return array(
            'divisionCode' => ''
        );
    }

    public function testDivisionCodeFilterInvalid()
    {
        $this->request->expects($this->once())->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsDivisionFilterInvalid')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        try {
            $response = $this->division->getDivisions();
            $this->fail();
        } catch (\Exception $e) {
            $this->assertEquals('Error: divisionCode is invalid', $e->getMessage());
        }
    }

    public function mockOptionsDivisionFilterInvalid()
    {
        return array(
            'divisionCode' => 'delete * from all_tables;'
        );
    }
}