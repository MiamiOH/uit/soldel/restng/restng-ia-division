#!perl

package StepConfig;
use strict;
use warnings;

use Data::Dumper;

use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw($server);

our $server = 'http://ws/api';

1;